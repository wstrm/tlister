# TSR Lister (tlister)

```
Usage of tlister:
  -jd string
    	Jingles directory (default "jingles")
  -jm string
    	Jingles M3U list
  -jo int
    	Jingles occurance, ex. 3 is for after every third song (default 3)
  -jr
    	Remove jingles if they exist in songs list
  -js
    	Shuffle the jingles
  -o string
    	M3U output file (default "./output.m3u")
  -rd string
    	Root directory (default "/media")
  -sd string
    	Songs directory (default "songs")
  -sm string
    	Songs M3U list
  -ss
    	Shuffle the songs (default true)
  -v	Print version and exit
```
