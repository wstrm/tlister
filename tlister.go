package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"time"
)

type flags struct {
	version        bool
	rootDir        string
	output         string
	songsDir       string
	songsM3U       string
	songsShuffle   bool
	jinglesDir     string
	jinglesM3U     string
	jinglesShuffle bool
	jinglesOcc     int
	jinglesRemove  bool
}

type sound struct {
	Sound string
}

type soundList struct {
	Sounds []sound
}

var context = flags{
	version:        false,
	rootDir:        "/media",
	output:         "./output.m3u",
	songsM3U:       "",
	songsShuffle:   true,
	songsDir:       "songs",
	jinglesM3U:     "",
	jinglesDir:     "jingles",
	jinglesShuffle: false,
	jinglesOcc:     3,
	jinglesRemove:  false,
}

var songList = soundList{}
var jingleList = soundList{}

func walkSound(list *soundList) filepath.WalkFunc {
	return func(path string, file os.FileInfo, err error) error {

		if err != nil {
			return err
		}

		if !file.IsDir() {
			list.Sounds = append(list.Sounds, sound{Sound: path})
		}

		return nil
	}
}

func readSound(list *soundList, path string) error {

	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		list.Sounds = append(list.Sounds, sound{Sound: scanner.Text()})
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}

func shuffleSound(list *soundList) error {
	for i := range list.Sounds {
		j := rand.Intn(len(list.Sounds))
		list.Sounds[i].Sound, list.Sounds[j].Sound = list.Sounds[j].Sound, list.Sounds[i].Sound
	}

	return nil
}

func injectSound(baseList soundList, injectList soundList, occurance int) soundList {

	newList := soundList{}
	numOcc := occurance
	indexInj := 0

	for i := 0; i < len(baseList.Sounds); i++ {

		newList.Sounds = append(newList.Sounds, baseList.Sounds[i])

		if i == numOcc-1 {
			// Reset index injection counter if reached end of injectList.Sounds
			if indexInj == len(injectList.Sounds) {
				indexInj = 0
			}

			newList.Sounds = append(newList.Sounds, injectList.Sounds[indexInj]) // Inject sound

			indexInj++
			numOcc += occurance
		}

	}

	return newList
}

func cleanList(baseList soundList, remList soundList) soundList {

	newList := soundList{}
	remSet := false

	for i := range baseList.Sounds {
		remSet = false

		for j := range remList.Sounds {
			if baseList.Sounds[i].Sound == remList.Sounds[j].Sound {
				remSet = true
				break
			}
		}

		if !remSet {
			newList.Sounds = append(newList.Sounds, baseList.Sounds[i])
		}
	}

	return newList
}

func writeM3U(outputPath string, outputList soundList) error {

	file, err := os.Create(outputPath)
	if err != nil {
		return err
	}

	for i := range outputList.Sounds {
		_, err = io.WriteString(file, outputList.Sounds[i].Sound+"\n")
		if err != nil {
			return err
		}
	}

	return nil
}

func init() {

	const version = "0.0.1-alpha"

	flag.StringVar(&context.rootDir, "rd", context.rootDir, "Root directory")
	flag.StringVar(&context.songsDir, "sd", context.songsDir, "Songs directory")
	flag.StringVar(&context.jinglesDir, "jd", context.jinglesDir, "Jingles directory")

	flag.StringVar(&context.jinglesM3U, "jm", context.jinglesM3U, "Jingles M3U list")
	flag.StringVar(&context.songsM3U, "sm", context.songsM3U, "Songs M3U list")

	flag.StringVar(&context.output, "o", context.output, "M3U output file")

	flag.BoolVar(&context.jinglesShuffle, "js", context.jinglesShuffle, "Shuffle the jingles")
	flag.BoolVar(&context.songsShuffle, "ss", context.songsShuffle, "Shuffle the songs")

	flag.BoolVar(&context.jinglesRemove, "jr", context.jinglesRemove, "Remove jingles if they exist in songs list")

	flag.BoolVar(&context.version, "v", context.version, "Print version and exit")

	flag.IntVar(&context.jinglesOcc, "jo", context.jinglesOcc, "Jingles occurance, ex. 3 is for after every third song")

	flag.Parse()

	// Print version and exit
	if context.version {
		fmt.Printf("%s\n", version)
		os.Exit(0)
	}

	// Also print line file:linenumber
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	// Prefix log output with "[tlister (<version>)]"
	log.SetPrefix("[\033[32mtlister\033[0m (" + version + ")] ")

	return
}

func main() {

	rand.Seed(time.Now().UTC().UnixNano())

	if context.jinglesM3U != "" {
		if err := readSound(&jingleList, context.jinglesM3U); err != nil {
			log.Fatal(err)
		}
	} else {
		if err := filepath.Walk(filepath.Join(context.rootDir, context.jinglesDir), walkSound(&jingleList)); err != nil {
			log.Fatal(err)
		}
	}

	if context.songsM3U != "" {
		if err := readSound(&songList, context.songsM3U); err != nil {
			log.Fatal(err)
		}
	} else {
		if err := filepath.Walk(filepath.Join(context.rootDir, context.songsDir), walkSound(&songList)); err != nil {
			log.Fatal(err)
		}
	}

	if context.songsShuffle {
		if err := shuffleSound(&songList); err != nil {
			log.Fatal(err)
		}
	}

	if context.jinglesShuffle {
		if err := shuffleSound(&jingleList); err != nil {
			log.Fatal(err)
		}
	}

	if context.jinglesRemove {
		songList = cleanList(songList, jingleList)
	}

	list := injectSound(songList, jingleList, context.jinglesOcc)
	if err := writeM3U(context.output, list); err != nil {
		log.Fatal(err)
	}

}
