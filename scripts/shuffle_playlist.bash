#!/bin/bash

# shuffle_playlist.bash creates a new playlist with tlister using a specified
# jingles M3U and then reads the current playlist used by mpd, it will then
# clean that playlist from jingles before shuffling and injecting it with
# the jingles defined in the M3U.

JINGLES_M3U=$1
PLAYLIST_M3U=/tmp/shpl_new-playlist.m3u
OUTPUT_M3U=/var/lib/mpd/playlists/shpl_gen-playlist.m3u

# Save current playlist.
mpc playlist -f "%file%" > $PLAYLIST_M3U

# Run tlister on the playlist and use the jingles M3U
tlister \
  -jm=$JINGLES_M3U \
  -jr \
  -js \
  -sm=$PLAYLIST_M3U \
  -ss \
  -o $OUTPUT_M3U

mpc random no # Make sure random is turned off.
mpc consume no # Make sure consume is off.
mpc repeat yes # Make sure everything goes in a loop.
mpc crop # Remove all files except the playing one.

OUTPUT_NAME=${OUTPUT_M3U##*/} # Get basename
OUTPUT_NAME=${OUTPUT_NAME%.*} # Remove extension
mpc load $(echo $OUTPUT_NAME) # Add the playlist

# Clean up
rm $PLAYLIST_M3U
